package com.isoft.intern.database;

import com.isoft.intern.constants.ConnectionStrings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class JDBCConnector {
    private static Connection conn;

    private static final Logger logger = LoggerFactory.getLogger(JDBCConnector.class);

    public JDBCConnector() {
        getInstance();
    };

    /**
     * Initializing the connection to the database.
     */
    private static Connection init() {
        ResourceBundle connectionProperties = ResourceBundle.getBundle(ConnectionStrings.bundleName);

        String url = connectionProperties.getString(ConnectionStrings.url);

        try {
            Class.forName(connectionProperties.getString(ConnectionStrings.driver));
        } catch (ClassNotFoundException ex) {
            logger.error("class not found", ex.getCause());
        }

        Properties props = new Properties();
        props.setProperty("user", connectionProperties.getString(ConnectionStrings.user));
        props.setProperty("password", connectionProperties.getString(ConnectionStrings.password));

        try {
            conn = DriverManager.getConnection(url, props);
        } catch (SQLException sql) {
            logger.error("The connection to the database was not successful", sql.getCause());
        }

        return conn;
    }

    /**
     * Getting the instance of the connection
     */
    private static Connection getInstance() {
        if(conn == null) {
            conn = init();
        }

        return conn;
    }

    public void initTables() {
        try(Statement stmt = conn.createStatement()) {
            String createMoviesQuery = "CREATE TABLE movies(\n" +
                    "   id serial PRIMARY KEY,\n" +
                    "   title VARCHAR (50) UNIQUE NOT NULL,\n" +
                    "   description VARCHAR (1000) NOT NULL,\n" +
                    "   running_time VARCHAR (1000) NOT NULL,\n" +
                    "   review VARCHAR (1000) NOT NULL\n" +
                    ");";

            String createGenresQuery = "CREATE TABLE genres (\n" +
                    "\tid serial PRIMARY KEY,\n" +
                    "\tname VARCHAR(50) NOT NULL\n" +
                    ");";

            String createFilmsGenresQuery = "CREATE TABLE films_genres (\n" +
                    "\tmovie_id int REFERENCES movies (id) ON UPDATE CASCADE ON DELETE CASCADE,\n" +
                    "\tgenre_id int REFERENCES genres (id) ON UPDATE CASCADE,\n" +
                    "\tCONSTRAINT films_genres_pkey PRIMARY KEY (movie_id, genre_id) \n" +
                    ");";

            stmt.executeUpdate(createMoviesQuery);
            logger.info("CREATED MOVIES SUCCESSFULLY");
            stmt.executeUpdate(createGenresQuery);
            logger.info("CREATED GENRES SUCCESSFULLY");
            stmt.executeUpdate(createFilmsGenresQuery);
            logger.info("CREATED MANY TO MANY TABLEx SUCCESSFULLY");
        } catch (SQLException e) {
            logger.error("Failed to create tables", e);
        }
     }

    /**
     * Executing query with given parameters
     * @param query query to execute
     * @param wildCardParams the passed params that substitute the question marks
     */
    public void executeQuery(String query, Object... wildCardParams) {
        try(PreparedStatement pstmt = conn.prepareStatement(query)) {
            for (int i = 0; i <wildCardParams.length; i++) {
                pstmt.setObject(i + 1, wildCardParams[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex ) {
            logger.error("wrong sql query", ex.getCause());
        }
    }

    /**
     * Executes a SELECT query with multiple results
     * every entity is represented as Map<String, Object>
     * where String is the column name, Object is the column value
     * if there are no entities from the result return empty List
     *
     * @param query the query
     * @return list of entities represented as described above
     */
    public List<Map<String, Object>> executeQueryWithMultipleResult(String query)
    {
        List<Map<String, Object>> objectsList = new ArrayList<>();
        try (Statement stmt = conn.createStatement();
             PreparedStatement ignored = conn.prepareStatement(query))
        {
            ResultSet r = stmt.executeQuery(query);
            objectsList = parseResultSetToMap(r);
            logger.info("Successfully fetched query: " + query);
        } catch (SQLException e) {
            logger.error("Failed to execute query: " + query, e);
        }

        return objectsList;
    }

    /**
     * @param r the result set which was received by the select query
     * @return the entities list
     */
    private List<Map<String, Object>> parseResultSetToMap(ResultSet r) throws SQLException
    {
        ResultSetMetaData metadata = r.getMetaData();
        int columnCount = metadata.getColumnCount();

        List<Map<String, Object>> entitiesList = new ArrayList<>();

        int index = 0;
        //For each entity (r.next) iterate all its columns
        //and put them in a map where key is column name
        //and value is column value
        while (r.next())
        {
            entitiesList.add(new HashMap<>());

            for (int i = 1; i <= columnCount; i++)
            {
                String col = metadata.getColumnName(i);
                Object value = r.getObject(col);
                entitiesList.get(index).put(col, value);
            }
            index++;
        }
        return entitiesList;
    }

    /**
     * execute a SELECT query where the result is only 1 entity
     *
     * @param query the query
     * @return a key - value pair -> column name - column value, empty map if no entity is found
     */
    public Map<String , Object> executeQueryWithSingleResult(String query) {
        Map<String, Object> resultColumnsMap = new HashMap<>();

        try(Statement stmt = conn.createStatement();
            PreparedStatement pstmt = conn.prepareStatement(query)){

            ResultSet r = stmt.executeQuery(query);

            resultColumnsMap = parseResultSetToMap(r).get(0);
            logger.info("Successfully fetched query: " + query);
            return resultColumnsMap;
        } catch (SQLException ex) {
            logger.error("Failed to execute query " + query, ex);
        }

        return resultColumnsMap;
    }
}