package com.isoft.intern.constants;

public final class ConnectionStrings {
    private ConnectionStrings() {}

    public final static String bundleName = "connection";

    public final static String url = "connection_url";

    public final static String driver = "connection_driver";

    public final static String user = "connection_user";

    public final static String password = "connection_password";
}
