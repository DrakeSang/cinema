package com.isoft.intern.services;

import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;

import java.util.Map;

public interface GenreService {
    void insertGenre(Genre genre);

    void deleteGenre(Integer genreId);

    void updateGenre(int id, Map<String, Object> genreParams);

    Movie getGenresByMovieId(int id);
}
