package com.isoft.intern.services.impl;

import com.isoft.intern.database.JDBCConnector;
import com.isoft.intern.entities.Movie;
import com.isoft.intern.repository.MovieRepository;
import com.isoft.intern.repository.impl.MovieRepositoryImpl;
import com.isoft.intern.services.MovieService;

import java.util.List;
import java.util.Map;

public class MovieServiceImpl implements MovieService {
    private static JDBCConnector db = new JDBCConnector();

    private static MovieRepository movieRepository = new MovieRepositoryImpl(db);

    public MovieServiceImpl() {};

    @Override
    public void insertMovie(Movie movie) {
        movieRepository.insert(movie);
    }

    @Override
    public void deleteMovie(Integer movieId) {
        movieRepository.delete(movieId);
    }

    @Override
    public void updateMovie(int id, Map<String, Object> movieParams) {
        movieRepository.update(id, movieParams);
    }

    @Override
    public List<Movie> getAll() {
        return movieRepository.getAll();
    }
}
