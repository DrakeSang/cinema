package com.isoft.intern.services.impl;

import com.isoft.intern.database.JDBCConnector;
import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;
import com.isoft.intern.repository.GenreRepository;
import com.isoft.intern.repository.impl.GenreRepositoryImpl;
import com.isoft.intern.services.GenreService;

import java.util.Map;

public class GenreServiceImpl implements GenreService {
    private static JDBCConnector db = new JDBCConnector();

    private static GenreRepository genreRepository = new GenreRepositoryImpl(db);

    public GenreServiceImpl() {};

    @Override
    public void insertGenre(Genre genre) {
        genreRepository.insert(genre);
    }

    @Override
    public void deleteGenre(Integer genreId) {
        genreRepository.delete(genreId);
    }

    @Override
    public void updateGenre(int id, Map<String, Object> genreParams) {
        genreRepository.update(id, genreParams);
    }

    @Override
    public Movie getGenresByMovieId(int id) {
        return genreRepository.getGenresByMovieId(id);
    }
}
