package com.isoft.intern.services;

import com.isoft.intern.entities.Movie;

import java.util.List;
import java.util.Map;

public interface MovieService {
    void insertMovie(Movie genre);

    void deleteMovie(Integer movieId);

    void updateMovie(int id, Map<String, Object> movieParams);

    List<Movie> getAll();
}
