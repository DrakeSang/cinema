package com.isoft.intern.servlets;

import com.isoft.intern.entities.Movie;
import com.isoft.intern.services.MovieService;
import com.isoft.intern.services.impl.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.isoft.intern.utils.RequestIOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class MovieServlet extends HttpServlet {
    private static final MovieService movieService = new MovieServiceImpl();

    private static Logger logger = LoggerFactory.getLogger(GenreServlet.class);

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Movie movie = RequestIOUtils.parseJSONtoObject(req, Movie.class);
        movieService.insertMovie(movie);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String movieId = req.getParameter("id");

        try {
            int id = Integer.parseInt(movieId);
            movieService.deleteMovie(id);
        } catch (NumberFormatException ex) {
            logger.error("wrong id format or missing", ex.getCause());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing id");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int movieId = Integer.parseInt(req.getParameter("id"));
        Map<String, Object> requestParams = RequestIOUtils.paramsToUpdateMapper(req, resp);

        movieService.updateMovie(movieId, requestParams);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Movie> list = movieService.getAll();
        RequestIOUtils.writeJSONResponse(resp, list);
    }
}