package com.isoft.intern.servlets;

import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;
import com.isoft.intern.services.GenreService;
import com.isoft.intern.services.impl.GenreServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.isoft.intern.utils.RequestIOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class GenreServlet extends HttpServlet {
    private static final GenreService genreService = new GenreServiceImpl();

    private static Logger logger = LoggerFactory.getLogger(GenreServlet.class);

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Genre genre = RequestIOUtils.parseJSONtoObject(req, Genre.class);
        genreService.insertGenre(genre);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String genreId = req.getParameter("id");

        try {
            int id = Integer.parseInt(genreId);
            genreService.deleteGenre(id);
        } catch (NumberFormatException ex) {
            logger.error("wrong id format or missing", ex.getCause());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing id");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int genreId = Integer.parseInt(req.getParameter("id"));
        Map<String, Object> requestParams = RequestIOUtils.paramsToUpdateMapper(req, resp);

        genreService.updateGenre(genreId, requestParams);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int movieId = Integer.parseInt(req.getParameter("movieId"));

        Movie movie = genreService.getGenresByMovieId(movieId);

        RequestIOUtils.writeJSONResponse(resp, movie.getGenreList());
    }
}