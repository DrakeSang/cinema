package com.isoft.intern.repository;

import java.util.Map;

public interface BaseRepository<T> {
    void insert(T entity);

    void delete(int id);

    void update(int id, Map<String, Object> movieParams);
}