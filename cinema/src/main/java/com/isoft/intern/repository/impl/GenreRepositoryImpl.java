package com.isoft.intern.repository.impl;

import com.isoft.intern.database.JDBCConnector;
import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;
import com.isoft.intern.repository.GenreRepository;
import com.isoft.intern.utils.mappers.DBRawToEntitiesMapper;
import com.isoft.intern.utils.storedEntitySQLS.GenrePredefinedSQL;
import com.isoft.intern.utils.storedEntitySQLS.MoviePredefinedSQL;
import com.isoft.intern.utils.storedEntitySQLS.impl.DefineSQL;

import java.util.Map;

public class GenreRepositoryImpl implements GenreRepository {
    private JDBCConnector db;

    private static DefineSQL defineSQL = new GenrePredefinedSQL();

    public GenreRepositoryImpl(JDBCConnector db) {
        this.db = db;
    }

    @Override
    public void insert(Genre entity) {
        db.executeQuery(GenrePredefinedSQL.SAVE, entity.getName());
    }

    @Override
    public void delete(int id) {
        db.executeQuery(GenrePredefinedSQL.DELETE, id);
    }

    @Override
    public void update(int id, Map<String, Object> genreParamsToUpdate) {
        String updateQuery = defineSQL.createUpdateQuery("genres", genreParamsToUpdate, id);

        db.executeQuery(updateQuery);
    }

    @Override
    public Movie getGenresByMovieId(int movieId) {
        String query = String.format(MoviePredefinedSQL.GET_SINGLE_MOVIE_WITH_GENRES, movieId);
        Map<String, Object> rawMovie = db.executeQueryWithSingleResult(query);

        return DBRawToEntitiesMapper.mapToMovieWithGenres(rawMovie);
    }
}