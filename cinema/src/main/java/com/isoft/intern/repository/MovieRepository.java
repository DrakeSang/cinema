package com.isoft.intern.repository;

import com.isoft.intern.entities.Movie;

import java.util.List;

public interface MovieRepository extends BaseRepository<Movie> {
    List<Movie> getAll();
}
