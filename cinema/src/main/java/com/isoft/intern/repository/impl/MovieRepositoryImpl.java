package com.isoft.intern.repository.impl;

import com.isoft.intern.entities.Movie;
import com.isoft.intern.repository.MovieRepository;
import com.isoft.intern.database.JDBCConnector;
import com.isoft.intern.utils.mappers.DBRawToEntitiesMapper;
import com.isoft.intern.utils.storedEntitySQLS.GenrePredefinedSQL;
import com.isoft.intern.utils.storedEntitySQLS.MoviePredefinedSQL;
import com.isoft.intern.utils.storedEntitySQLS.impl.DefineSQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MovieRepositoryImpl implements MovieRepository {
    private JDBCConnector db;

    private static DefineSQL defineSQL = new GenrePredefinedSQL();

    public MovieRepositoryImpl(JDBCConnector db) {
        this.db = db;
    }

    @Override
    public void insert(Movie entity) {
        db.executeQuery(MoviePredefinedSQL.SAVE,
                entity.getTitle(),
                entity.getDescription(),
                entity.getRunning_time(),
                entity.getReview());

        db.executeQuery(
                MoviePredefinedSQL.SAVE_MOVIE_GENRE,
                entity.getTitle(),
                "Action");

        db.executeQuery(
                MoviePredefinedSQL.SAVE_MOVIE_GENRE,
                entity.getTitle(),
                "Adventure");

        db.executeQuery(
                MoviePredefinedSQL.SAVE_MOVIE_GENRE,
                entity.getTitle(),
                "Sci-Fi");
    }

    @Override
    public void delete(int id) {
        db.executeQuery(MoviePredefinedSQL.DELETE, id);
    }

    @Override
    public void update(int id, Map<String, Object> movieParamsToUpdate) {
        String updateQuery = defineSQL.createUpdateQuery("movies", movieParamsToUpdate, id);

        db.executeQuery(updateQuery);
    }

    @Override
    public List<Movie> getAll() {
        List<Map<String, Object>> listOfRawPosts = db.executeQueryWithMultipleResult(MoviePredefinedSQL.GET_ALL);

        List<Movie> mappedMovies = new ArrayList<>();
        for(Map<String, Object> singleMovie : listOfRawPosts) {
            mappedMovies.add(DBRawToEntitiesMapper.mapToMovieWithGenres(singleMovie));
        }

        return mappedMovies;
    }
}