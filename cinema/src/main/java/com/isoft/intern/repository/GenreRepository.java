package com.isoft.intern.repository;

import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;

public interface GenreRepository extends BaseRepository<Genre> {
    Movie getGenresByMovieId(int movieId);
}