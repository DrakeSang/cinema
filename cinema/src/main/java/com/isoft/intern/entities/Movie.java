package com.isoft.intern.entities;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private int id;

    private String title;

    private String description;

    private String running_time;

    private String review;

    private List<Genre> genreList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRunning_time() {
        return running_time;
    }

    public void setRunning_time(String running_time) {
        this.running_time = running_time;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(Genre genre) {
        this.genreList.add(genre);
    }
}
