package com.isoft.intern.utils.storedEntitySQLS;

public class MoviePredefinedSQL {
    public static final String SAVE = "INSERT INTO movies (title, description, running_time, review)\n" +
            "VALUES (?, ?, ?, ?)";

    public static String SAVE_MOVIE_GENRE = "INSERT INTO films_genres(movie_id, genre_id) \n" +
            "VALUES \n" +
            "(\n" +
            "\t(SELECT id FROM movies WHERE title = ?),\n" +
            "\t(SELECT id FROM genres WHERE name = ?)\n" +
            ")";

    public static String DELETE = "DELETE FROM movies WHERE id = ?";

    public static String GET_ALL = "SELECT movies.*, string_agg(genres.name, ',') as genres\n" +
            "FROM movies\n" +
            "INNER JOIN films_genres\n" +
            "ON movies.id = films_genres.movie_id\n" +
            "INNER JOIN genres\n" +
            "ON genres.id = films_genres.genre_id\n" +
            "GROUP BY movies.id";

    public static String GET_SINGLE_MOVIE_WITH_GENRES = "SELECT movies.*, string_agg(genres.name, ',') as genres\n" +
            "FROM movies\n" +
            "INNER JOIN films_genres\n" +
            "ON movies.id = films_genres.movie_id\n" +
            "INNER JOIN genres\n" +
            "ON genres.id = films_genres.genre_id\n" +
            "GROUP BY movies.id\n" +
            "HAVING movies.id = %d";
}