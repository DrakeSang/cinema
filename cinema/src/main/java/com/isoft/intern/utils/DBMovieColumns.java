package com.isoft.intern.utils;

public class DBMovieColumns {
    /**
     * Representing the columns of the movie,where the id,title,... are the names of
     * the columns
     */
    public static final String MOVIE_ID = "id";

    public static final String MOVIE_TITLE = "title";

    public static final String MOVIE_DESCRIPTION = "description";

    public static final String MOVIE_RUNNING_TIME = "running_time";

    public static final String MOVIE_REVIEW = "review";
}
