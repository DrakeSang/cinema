package com.isoft.intern.utils;

public class DBGenreColumns {
    /**
     * Representing the columns of the genre,where the id,name,... are the names of
     * the columns
     */
    public static final String GENRE_ID = "id";

    public static final String GENRE_NAME = "name";
}
