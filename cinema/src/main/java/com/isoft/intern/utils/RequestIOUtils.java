package com.isoft.intern.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestIOUtils {
    private static Logger logger = LoggerFactory.getLogger(RequestIOUtils.class);

    private RequestIOUtils() {

    }

    /**
     * parse the given entity to JSON then write it to the response's output stream
     *
     * @param resp the response
     * @param object the entity
     * @throws IOException if there is an error with the stream
     */
    public static void writeJSONResponse(HttpServletResponse resp, Object object) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ObjectMapper mapper = new ObjectMapper();

        mapper.writeValue(out, object);

        final byte[] data = out.toByteArray();

        PrintWriter respOut = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        respOut.print(new String(data));
        respOut.flush();
    }

    /**
     * parse the servlet request's to entity
     *
     * @param req         the request with the json data
     * @param objectClass the class of the expected entity
     * @param <T>         the entity type
     * @return the parsed entity
     * @throws IOException if there is a problem with request's BufferedReader
     */
    public static <T> T parseJSONtoObject(HttpServletRequest req, Class<T> objectClass) throws IOException
    {
        BufferedReader r = req.getReader();
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = r.readLine()) != null)
        {
            sb.append(line);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

        return mapper.readValue(sb.toString(), objectClass);
    }

    public static Map<String, Object> paramsToUpdateMapper(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, Object> requestParams = new HashMap<>();
        String requestData = null;
        try {
            requestData = req.getReader().lines().collect(Collectors.joining());
            JSONObject movieASJsonObject = new JSONObject(requestData);

            requestParams = new HashMap<>();
            JSONArray names = movieASJsonObject.names();

            for (int i = 0; i < names.length(); i++) {
                String key = names.getString(i);
                String value = movieASJsonObject.getString(key);

                requestParams.put(key, value);
            }
        } catch (IOException | JSONException e) {
            logger.error("wrong or missing value", e.getCause());
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "wrong or missing value");
        }

        return requestParams;
    }
}