package com.isoft.intern.utils.storedEntitySQLS;

import com.isoft.intern.utils.storedEntitySQLS.impl.DefineSQL;

public class GenrePredefinedSQL implements DefineSQL {
    public GenrePredefinedSQL() {

    }

    public static final String SAVE = "INSERT INTO genres (name) VALUES (?)";

    public static final String DELETE = "DELETE FROM genres WHERE id = ?";

    public static final String FIND_GENRE_BY_NAME = "SELECT * FROM genres WHERE name = '%s'";
}
