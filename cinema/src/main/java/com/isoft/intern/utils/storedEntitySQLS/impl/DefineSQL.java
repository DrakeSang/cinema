package com.isoft.intern.utils.storedEntitySQLS.impl;

import java.util.Map;

public interface DefineSQL {
    default String createUpdateQuery(String tableName, Map<String, Object> paramsToUpdate, int entityId) {
        String query = "UPDATE " + tableName + " SET ";

        String[] columns = paramsToUpdate.keySet().toArray(new String[0]);
        for (int i = 0; i < columns.length; i++)
        {
            if(!columns[i].equals("id")) {
                query += columns[i] + " = " + '\'' + paramsToUpdate.get(columns[i]) + '\'';

                if (i < columns.length - 1)
                    query += " , ";
            }
        }

        query += " WHERE id = " + entityId;

        return query;
    }
}
