package com.isoft.intern.utils.mappers;

import com.isoft.intern.database.JDBCConnector;
import com.isoft.intern.entities.Genre;
import com.isoft.intern.entities.Movie;
import com.isoft.intern.utils.DBGenreColumns;
import com.isoft.intern.utils.DBMovieColumns;
import com.isoft.intern.utils.storedEntitySQLS.GenrePredefinedSQL;
import java.util.Map;

public class DBRawToEntitiesMapper {
    private static JDBCConnector db = new JDBCConnector();

    private DBRawToEntitiesMapper() {}

    /**
     * Returning movie as entity
     * @param rawMovie movie as raw object
     */
    public static Movie mapToMovieWithGenres(Map<String, Object> rawMovie) {
        Movie movie = new Movie();

        movie.setId((Integer) rawMovie.get(DBMovieColumns.MOVIE_ID));
        movie.setTitle((String) rawMovie.get(DBMovieColumns.MOVIE_TITLE));
        movie.setDescription((String) rawMovie.get(DBMovieColumns.MOVIE_DESCRIPTION));
        movie.setRunning_time((String) rawMovie.get(DBMovieColumns.MOVIE_RUNNING_TIME));
        movie.setReview((String) rawMovie.get(DBMovieColumns.MOVIE_REVIEW));

        String genres = (String) rawMovie.get("genres");
        String[] genreNames = genres.split(",");

        for (String currentGenreName : genreNames) {
            String query = String.format(GenrePredefinedSQL.FIND_GENRE_BY_NAME, currentGenreName);
            Map<String, Object> rawGenre = db.executeQueryWithSingleResult(query);
            Genre genre = mapToGenre(rawGenre);
            movie.setGenreList(genre);
        }

        return movie;
    }

    private static Genre mapToGenre(Map<String, Object> rawGenre) {
        Genre genre = new Genre();

        genre.setId((Integer) rawGenre.get(DBGenreColumns.GENRE_ID));
        genre.setName((String) rawGenre.get(DBGenreColumns.GENRE_NAME));

        return genre;
    }
}